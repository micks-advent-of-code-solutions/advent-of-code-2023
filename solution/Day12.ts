import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

const springGroupRex = /#+/g;
function matches(pattern: string, numbers: number[]) {
  const matches = Array.from(pattern.matchAll(springGroupRex));
  return (
    matches.length === numbers.length &&
    matches.every((match, i) => match[0].length === numbers[i])
  );
}

function numberOfSetBits(i: number) {
  i = (i | 0) - ((i >> 1) & 0x55555555); // add pairs of bits
  i = (i & 0x33333333) + ((i >> 2) & 0x33333333); // quads
  i = (i + (i >> 4)) & 0x0f0f0f0f; // groups of 8
  i *= 0x01010101; // horizontal sum of bytes
  return i >> 24; // return just that top byte (after truncating to 32-bit even when int is wider than uint32_t)
}

function getPermutationCount(pattern: string, numbers: number[]) {
  const spotsToReplace = [];
  let permutations = 0;
  const minCount =
    numbers.reduce(reduceSum) - pattern.replace(/[^#]/g, "").length;

  for (let i = 0; i < pattern.length; i++) {
    if (pattern[i] === "?") spotsToReplace.push(i);
  }
  const spotCount = spotsToReplace.length;
  const permutationCount = 2 ** spotCount;

  for (let i = 0; i < permutationCount; i++) {
    if (numberOfSetBits(i) < minCount) continue;
    const patternArr = pattern.split("");
    for (let j = 0; j < spotCount; j++) {
      patternArr[spotsToReplace[j]] = i & (1 << j) ? "#" : ".";
    }
    if (matches(patternArr.join(""), numbers)) permutations++;
  }

  return permutations;
}

export default class Day12 extends Solution {
  async prepareData(input: string, isSilver: boolean) {
    return input.split("\n").map((line) => {
      const [pattern, numbersStr] = line.split(" ");
      const numbers = (
        isSilver
          ? numbersStr
          : [numbersStr, numbersStr, numbersStr, numbersStr, numbersStr].join(
              ",",
            )
      )
        .split(",")
        .map((i) => parseInt(i));

      return { pattern: isSilver ? pattern : pattern.repeat(4), numbers };
    });
  }

  async solveSilver(data: Awaited<ReturnType<Day12["prepareData"]>>) {
    return (
      data
        //.slice(0, 100)
        .map(({ pattern, numbers }) => {
          return getPermutationCount(pattern, numbers);
        })
        .reduce(reduceSum)
    );
  }

  async solveGold(data: Awaited<ReturnType<Day12["prepareData"]>>) {
    return null;
    return data
      .slice(0, 1)
      .map(({ pattern, numbers }) => {
        console.log(pattern, numbers);
        return getPermutationCount(pattern, numbers);
      })
      .reduce(reduceSum);
  }
}
