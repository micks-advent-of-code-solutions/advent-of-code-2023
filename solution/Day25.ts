import Solution from "./Solution.js";
import { groupBy } from "./utils/List.js";
import { reduceProduct } from "./utils/Math.js";

function visit(
  visitor: string,
  currentNodeId: string,
  nodes: Awaited<ReturnType<Day25["prepareData"]>>,
) {
  const currentNode = nodes[currentNodeId];

  if (currentNode.visitedBy) return;
  currentNode.visitedBy = visitor;
  currentNode.links.forEach((node) => visit(visitor, node, nodes));
}

export default class Day25 extends Solution {
  async prepareData(input: string) {
    const nodes = {} as Record<
      string,
      { node: string; links: string[]; visitedBy: string }
    >;

    input.split("\n").forEach((line) => {
      const [node, linksStr] = line.split(": ");
      const links = linksStr.split(" ");
      if (!nodes[node]) {
        nodes[node] = { node, links: [], visitedBy: null as string };
      }
      nodes[node].links = nodes[node].links.concat(links);
      for (const link of links) {
        if (!nodes[link]) {
          nodes[link] = { node: link, links: [node], visitedBy: null };
        }
        if (!nodes[link].links.includes(node)) {
          nodes[link].links.push(node);
        }
      }
    });

    return nodes;
  }

  async solveSilver(data: Awaited<ReturnType<Day25["prepareData"]>>) {
    for (const [node, nodeData] of Object.entries(data)) {
      if (nodeData.visitedBy) continue;
      console.log(node, nodeData);
      visit(node, node, data);
    }

    console.log(data.bvb);

    return [
      ...groupBy(Object.values(data), (entry) => entry.visitedBy).values(),
    ]
      .map((e) => e.length)
      .reduce(reduceProduct);
  }

  async solveGold() {
    return null;
  }
}
