import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

function hash(stringToHash: string) {
  let value = 0;
  for (let i = 0; i < stringToHash.length; i++) {
    value = ((value + stringToHash.charCodeAt(i)) * 17) % 256;
  }
  return value;
}

export default class Day15 extends Solution {
  async prepareData(input: string) {
    return input.split(",").map((instruction) => {
      const {
        groups: { label, action, param },
      } = instruction.match(/(?<label>\w+)(?<action>[=-])(?<param>\d)?/);

      return {
        instruction,
        label,
        box: hash(label),
        action: action === "=" ? "set" : "delete",
        param: param ? parseInt(param) : 0,
      };
    });
  }

  async solveSilver(data: Awaited<ReturnType<Day15["prepareData"]>>) {
    return data.map(({ instruction }) => hash(instruction)).reduce(reduceSum);
  }

  async solveGold(data: Awaited<ReturnType<Day15["prepareData"]>>) {
    const boxes = new Map<number, Map<string, number>>();
    data.forEach((instruction) => {
      if (!boxes.has(instruction.box)) {
        boxes.set(instruction.box, new Map());
      }
      const box = boxes.get(instruction.box);
      switch (instruction.action) {
        case "set":
          box.set(instruction.label, instruction.param);
          break;

        case "delete":
          box.delete(instruction.label);
          break;
      }
    });
    console.log(boxes);

    let value = 0;
    for (const [boxId, box] of boxes.entries()) {
      const valueCount = box.size;
      const values = Array.from(box.values());

      for (let i = 0; i < valueCount; i++) {
        value += (boxId + 1) * (i + 1) * values[i];
      }
    }
    return value;
  }
}
