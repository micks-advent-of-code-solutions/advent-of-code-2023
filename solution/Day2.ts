import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

export default class Day2 extends Solution {
  async prepareData(input: string) {
    return input.split("\n").map((line) => {
      const [game, content] = line.split(": ");
      const rolls = content.split("; ").map((entry) =>
        Object.fromEntries(
          entry.split(", ").map((i) => {
            const [count, color] = i.split(" ");
            return [color, parseInt(count)];
          }),
        ),
      );
      return {
        id: parseInt(game.substring(5)),
        rolls,
      };
    });
  }

  async solveSilver(data: Awaited<ReturnType<Day2["prepareData"]>>) {
    const rollCheck = { red: 12, green: 13, blue: 14 };

    return data
      .filter((game) =>
        game.rolls.every((roll) =>
          Object.entries(roll).every(
            ([color, count]) => rollCheck[color] >= count,
          ),
        ),
      )
      .reduce((a, b) => a + b.id, 0);
  }

  async solveGold(data: Awaited<ReturnType<Day2["prepareData"]>>) {
    return data
      .map((game) => {
        const maxCount = { red: 0, green: 0, blue: 0 };
        game.rolls.forEach((roll) =>
          Object.entries(roll).forEach(
            ([color, count]) =>
              (maxCount[color] = Math.max(maxCount[color], count)),
          ),
        );
        return Object.values(maxCount).reduce((a, b) => a * b);
      })
      .reduce(reduceSum);
  }
}
