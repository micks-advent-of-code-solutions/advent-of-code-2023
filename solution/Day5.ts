import Solution from "./Solution.js";

class Mapping {
  ranges: Array<{ start: number; end: number; distance: number }> = [];

  addRange(startDestination, startSource, length) {
    this.ranges.unshift({
      start: startSource,
      end: startSource + length,
      distance: startDestination - startSource,
    });
  }

  get(i: number) {
    for (const { start, end, distance } of this.ranges) {
      if (i >= start && i <= end) {
        return i + distance;
      }
    }
    return i;
  }

  reverseGet(i: number) {
    for (const { start, end, distance } of this.ranges) {
      if (i >= start + distance && i <= end + distance) {
        return i - distance;
      }
    }
    return i;
  }
}

export default class Day5 extends Solution {
  async prepareData(input: string) {
    const [rawSeeds, ...rawMaps] = input.split("\n\n");

    const seeds = rawSeeds
      .split("seeds: ")
      .pop()
      .split(" ")
      .map((num) => parseInt(num));

    const maps = rawMaps.map((mapBlock) => {
      const map = new Mapping();
      mapBlock
        .split("\n")
        .slice(1)
        .forEach((mapLine) => {
          const [startDestination, startSource, length] = mapLine
            .split(" ")
            .map((x) => parseInt(x));
          map.addRange(startDestination, startSource, length);
        });
      return map;
    });

    return {
      seeds,
      maps,
    };
  }

  async solveSilver(data: Awaited<ReturnType<Day5["prepareData"]>>) {
    let smallestNumber = Number.MAX_SAFE_INTEGER;

    for (const seed of data.seeds) {
      let value = seed;
      for (const map of data.maps) {
        value = map.get(value);
      }
      if (value < smallestNumber) {
        smallestNumber = value;
      }
    }

    return smallestNumber;
  }

  async solveGold(data: Awaited<ReturnType<Day5["prepareData"]>>) {
    const seedRanges = [];
    for (let i = 0; i < data.seeds.length; i += 2) {
      const start = data.seeds[i];
      const range = data.seeds[i + 1];
      const end = start + range;
      seedRanges.push([start, end]);
    }

    const reversedMaps = data.maps.toReversed();

    let test = 0;
    while (test < Number.MAX_SAFE_INTEGER) {
      let value = test;
      for (const map of reversedMaps) {
        value = map.reverseGet(value);
      }

      for (const [start, end] of seedRanges) {
        if (value >= start && value <= end) {
          return test;
        }
      }

      test++;
    }
  }
}
