import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

const pureStateTransition = (nextState) => () => nextState;

const conditionalTransitions = {
  ">": (property, value, nextState) => (state: StateData) =>
    state[property] > value ? nextState : false,
  "<": (property, value, nextState) => (state: StateData) =>
    state[property] < value ? nextState : false,
};

function mapTransition(transitionString: string) {
  if (transitionString.includes(":")) {
    const match = transitionString.match(
      /(?<property>\w)(?<comparator>[<>])(?<value>\d+):(?<nextState>\w+)/,
    ).groups;
    return conditionalTransitions[match.comparator](
      match.property,
      match.value,
      match.nextState,
    );
  } else {
    return pureStateTransition(transitionString);
  }
}

type StateData = { x: number; m: number; a: number; s: number };
type StateTransitions = Record<string, Array<(state: StateData) => string>>;

class StateMachine {
  stateTransitions: StateTransitions;

  constructor(stateTransitions: StateTransitions) {
    this.stateTransitions = stateTransitions;
  }

  findNextState(current: string, stateData: StateData) {
    const stateTransition = this.stateTransitions[current];
    for (const test of stateTransition) {
      const nextState = test(stateData);
      if (nextState) return nextState;
    }
  }

  run(stateData: StateData) {
    let current = "in";
    while (current !== "A" && current !== "R") {
      current = this.findNextState(current, stateData);
    }
    return current;
  }
}

export default class Day19 extends Solution {
  async prepareData(input: string) {
    const [stateMachineLines, stateLines] = input
      .split("\n\n")
      .map((l) => l.split("\n"));

    const states = stateLines.map((l) =>
      Object.fromEntries(
        [...l.matchAll(/(?<key>[xmas])=(?<value>\d+)/g)].map((m) => [
          m.groups.key,
          parseInt(m.groups.value),
        ]),
      ),
    ) as Array<StateData>;

    const stateTransitions = Object.fromEntries(
      stateMachineLines.map((line) => {
        const { name, transitionString } = line.match(
          /(?<name>\w+){(?<transitionString>.*?)}/,
        ).groups;

        return [name, transitionString.split(",").map(mapTransition)];
      }),
    );

    return { stateTransitions, states };
  }

  async solveSilver(data: Awaited<ReturnType<Day19["prepareData"]>>) {
    const machine = new StateMachine(data.stateTransitions);

    return data.states
      .filter((state) => machine.run(state) === "A")
      .map((s) => Object.values(s))
      .flat()
      .reduce(reduceSum);
  }

  async solveGold() {
    return false;
  }
}
