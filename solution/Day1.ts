import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

export default class Day1 extends Solution {
  async prepareData(input: string, isSilver: boolean, isGold: boolean) {
    const writtenNumbers = "on|tw|thre|four|fiv|six|seve|eigh|nin".split("|");
    return input.split("\n").map((line) => {
      let modifiedLine = line;
      if (isGold) {
        modifiedLine = line.replace(
          /(on(?=e)|tw(?=o)|thre(?=e)|four|fiv(?=e)|six|seve(?=n)|eigh(?=t)|nin(?=e))/g,
          (match) => String(writtenNumbers.indexOf(match) + 1),
        );
      }
      const numbers = modifiedLine
        .split("")
        .filter((char) => char >= "1" && char <= "9");
      return parseInt(numbers.at(0) + numbers.at(-1));
    });
  }

  async solveSilver(data: Awaited<ReturnType<Day1["prepareData"]>>) {
    return data.reduce(reduceSum);
  }

  async solveGold(data: Awaited<ReturnType<Day1["prepareData"]>>) {
    return data.reduce(reduceSum);
  }
}
