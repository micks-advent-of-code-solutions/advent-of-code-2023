import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

import { groupBy } from "./utils/common.js";

function getCardValue(card: string, useJokers = false) {
  const parsed = parseInt(card);
  if (parsed) return parsed;
  if (useJokers && card === "J") return 1;
  return "TJQKA".indexOf(card) + 10;
}

function getCardValues(hand: string[], useJokers = false) {
  return hand.map((i) => getCardValue(i, useJokers));
}

const handNames = [
  "high card",
  "one pair",
  "two pairs",
  "three of a kind",
  "full house",
  "four of a kind",
  "five of a kind",
];

function getHandValue(sortedCardValues: number[]) {
  const groups = groupBy(sortedCardValues);
  const jokers = groups.get(1)?.length ?? 0;
  groups.delete(1);

  if (groups.size === 1) {
    // five of a kind
    return 6;
  }

  if (groups.size === 4) {
    // one pair
    return 1;
  }

  if (groups.size === 5) {
    // high card
    return 0;
  }

  const biggestGroup = [...groups.values()]
    .map((g) => g.length)
    .toSorted((a, b) => b - a)[0];

  if (groups.size === 2) {
    // full house oder four of a kind
    if (biggestGroup + jokers === 3) {
      // full house
      return 4;
    } else {
      // four of a kind
      return 5;
    }
  }

  if (groups.size === 3) {
    // 2 pair oder three of a kind
    if (biggestGroup + jokers === 3) {
      // three of a kind
      return 3;
    } else {
      // two pair
      return 2;
    }
  }
}

export default class Day7 extends Solution {
  async prepareData(input: string, isSilver) {
    return input.split("\n").map((line) => {
      const [cardsStr, bid] = line.split(" ");
      const cards = cardsStr.split("");
      const values = getCardValues(cards, !isSilver);
      const value = getHandValue(values);
      return { cards: cards, values, value, bid: parseInt(bid) };
    });
  }

  async solveSilver(data: Awaited<ReturnType<Day7["prepareData"]>>) {
    return data
      .toSorted((a, b) => {
        if (a.value !== b.value) return a.value - b.value;
        for (let i = 0; i < 5; i++) {
          if (a.values[i] !== b.values[i]) return a.values[i] - b.values[i];
        }
        return 0;
      })
      .map((entry, i) => entry.bid * (i + 1))
      .reduce(reduceSum);
  }

  async solveGold(data: Awaited<ReturnType<Day7["prepareData"]>>) {
    return data
      .toSorted((a, b) => {
        if (a.value !== b.value) return a.value - b.value;
        for (let i = 0; i < 5; i++) {
          if (a.values[i] !== b.values[i]) return a.values[i] - b.values[i];
        }
        return 0;
      })
      .map((entry) => {
        console.log(entry.cards, entry.values, handNames[entry.value]);
        return entry;
      })
      .map((entry, i) => entry.bid * (i + 1))
      .reduce(reduceSum);
  }
}
