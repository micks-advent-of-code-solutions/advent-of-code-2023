import Solution from "./Solution.js";

type Direction = "N" | "E" | "S" | "W";

function oppositeDirection(direction: Direction) {
  return { N: "S", S: "N", E: "W", W: "E" }[direction] as Direction;
}

export default class Day10 extends Solution {
  async prepareData(input: string) {
    return input
      .replaceAll(".", " ")
      .replaceAll("F", "┌")
      .replaceAll("7", "┐")
      .replaceAll("J", "┘")
      .replaceAll("L", "└")
      .replaceAll("-", "─")
      .replaceAll("|", "│");
  }

  async solveSilver(/*data: Awaited<ReturnType<Day10["prepareData"]>>*/) {
    return oppositeDirection("N");
  }

  // async solveGold(data: Awaited<ReturnType<Day10["prepareData"]>>) {
  //   return "Not solved Yet";
  // }
}
