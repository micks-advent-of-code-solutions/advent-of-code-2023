import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";
import { Vector2, toId } from "./utils/Vector.js";

function drawGrid(
  width: number,
  height: number,
  cubeRockPositions: Set<string>,
  roundRockPositions: Set<string>,
) {
  for (let y = 0; y < height; y++) {
    const line = Array(width);
    for (let x = 0; x < width; x++) {
      const posId = toId({ x, y });
      line[x] = cubeRockPositions.has(posId)
        ? "#"
        : roundRockPositions.has(posId)
          ? "O"
          : " ";
    }
    console.log(line.join(""));
  }
}

export default class Day14 extends Solution {
  async prepareData(input: string) {
    const cubeRocks: Vector2[] = [];
    const roundRocks: Vector2[] = [];

    const lines = input.split("\n");
    lines.forEach((line, y) => {
      line.split("").forEach((char, x) => {
        const position: Vector2 = { x, y };
        if (char === "#") cubeRocks.push(position);
        else if (char === "O") roundRocks.push(position);
      });
    });

    return {
      width: lines[0].length,
      height: lines.length,
      roundRocks,
      cubeRocks,
    };
  }

  async solveSilver({
    width,
    height,
    cubeRocks,
    roundRocks,
  }: Awaited<ReturnType<Day14["prepareData"]>>) {
    const cubeRockPositions = new Set(cubeRocks.map(toId));
    let roundRockPositions = new Set(roundRocks.map(toId));
    for (let i = 0; i < Number.MAX_SAFE_INTEGER; i++) {
      let couldMove = false;
      const newRockPositions = new Set<string>();
      console.log("\nIteration", i, "\n");
      //drawGrid(width, height, cubeRockPositions, roundRockPositions);
      for (const rock of roundRocks) {
        const newY = rock.y - 1;
        const newPosition = { x: rock.x, y: newY } as Vector2;
        const newPosId = toId(newPosition);
        if (
          newY > -1 &&
          !cubeRockPositions.has(newPosId) &&
          !roundRockPositions.has(newPosId)
        ) {
          // free to move
          rock.y = newY;
          newRockPositions.add(newPosId);
          couldMove = true;
        } else {
          newRockPositions.add(toId(rock));
        }
      }
      roundRockPositions = newRockPositions;
      if (!couldMove) {
        console.log("\n\nRESULT\n");
        drawGrid(width, height, cubeRockPositions, roundRockPositions);
        break;
      }
    }
    // calculate load
    return roundRocks.map((rock) => height - rock.y).reduce(reduceSum);
  }

  async solveGold(/*data: Awaited<ReturnType<Day14["prepareData"]>>*/) {
    return null;
  }
}
