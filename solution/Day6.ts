import Solution from "./Solution.js";

export default class Day6 extends Solution {
  async prepareData(input: string, isSilver: boolean) {
    const rex = isSilver ? /\d+/g : /[0-9 ]+/g;
    const [times, distances] = input
      .split("\n")
      .map((line) =>
        [...line.matchAll(rex)].map((match) =>
          parseInt(match[0].replace(/ /g, "")),
        ),
      );

    return Array.from(Array(times.length)).map((_, i) => ({
      time: times[i],
      distance: distances[i],
    }));
  }

  async solveSilver(data: Awaited<ReturnType<Day6["prepareData"]>>) {
    return data
      .map(({ time, distance }) => {
        let wins = 0;
        for (let i = 1; i < time; i++) {
          if ((time - i) * i > distance) wins++;
        }
        return wins;
      })
      .reduce((a, b) => a * b);
  }

  async solveGold(data: Awaited<ReturnType<Day6["prepareData"]>>) {
    return this.solveSilver(data);
  }
}
