import Solution from "./Solution.js";
import { LCM } from "./utils/Math.js";

function getSteps(
  graph: Map<string, { L: string; R: string }>,
  directions: Array<"L" | "R">,
  startNode: string,
  endNodes: Set<string>,
) {
  let current = startNode;
  let steps = 0;
  let directionIndex = 0;
  const directionCount = directions.length;

  while (!endNodes.has(current)) {
    const direction = directions[directionIndex];
    current = graph.get(current)[direction];
    steps++;
    directionIndex = (directionIndex + 1) % directionCount;
  }

  return steps;
}

export default class Day8 extends Solution {
  async prepareData(input: string) {
    const [directionStr, networkStr] = input.split("\n\n");

    const graph = new Map<string, { L: string; R: string }>();

    const matches = networkStr.matchAll(
      /(?<key>\w+) = \((?<L>\w+), (?<R>\w+)\)/g,
    );

    for (const { groups } of matches) {
      graph.set(groups.key, { L: groups.L, R: groups.R });
    }

    return { directions: directionStr.split("") as Array<"L" | "R">, graph };
  }

  async solveSilver({
    graph,
    directions,
  }: Awaited<ReturnType<Day8["prepareData"]>>) {
    return getSteps(graph, directions, "AAA", new Set(["ZZZ"]));
  }

  async solveGold({
    graph,
    directions,
  }: Awaited<ReturnType<Day8["prepareData"]>>) {
    const startNodes = [...graph.keys()].filter((node) => node.at(-1) === "A");

    const endNodes = new Set(
      [...graph.keys()].filter((node) => node.at(-1) === "Z"),
    );

    const steps = startNodes.map((node) =>
      getSteps(graph, directions, node, endNodes),
    );

    return steps.reduce(LCM);
  }
}
