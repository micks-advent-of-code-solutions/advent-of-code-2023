import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

function solveData(entries: number[], previous = false) {
  let currentEntries = entries;
  const idx = previous ? 0 : -1;
  const values = [entries.at(idx)];
  let nonZero = true;
  while (nonZero) {
    nonZero = false;
    const nextEntries = [];
    for (let i = 0; i < currentEntries.length - 1; i++) {
      const value = currentEntries[i + 1] - currentEntries[i];
      nextEntries.push(value);
      if (value !== 0) nonZero = true;
    }
    values.push(nextEntries.at(idx));
    currentEntries = nextEntries;
  }
  if (previous) return values.reduceRight((a, b) => b - a);
  return values.reduce(reduceSum);
}

export default class Day9 extends Solution {
  async prepareData(input: string) {
    return input
      .split("\n")
      .map((line) => line.split(" ").map((i) => parseInt(i)));
  }

  async solveSilver(data: Awaited<ReturnType<Day9["prepareData"]>>) {
    return data.map((i) => solveData(i)).reduce(reduceSum);
  }

  async solveGold(data: Awaited<ReturnType<Day9["prepareData"]>>) {
    return data.map((i) => solveData(i, true)).reduce(reduceSum);
  }
}
