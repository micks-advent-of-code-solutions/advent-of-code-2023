import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";

function coordKey(x: number, y: number) {
  return x + "/" + y;
}

export default class Day3 extends Solution {
  async prepareData(input: string) {
    const lines = input.split("\n");

    const gears = [];

    const symbols = new Set(
      lines
        .map((line, y) =>
          [...line.matchAll(/([^0-9.])/g)].map((match) => {
            if (match[0] === "*") gears.push({ x: match.index, y });
            return coordKey(match.index, y);
          }),
        )
        .flat(),
    );

    const numbers = lines
      .map((line, y) =>
        [...line.matchAll(/([0-9]+)/g)].map((match) => ({
          x: match.index,
          y,
          width: match[0].length,
          number: parseInt(match[0]),
        })),
      )
      .flat();

    return { numbers, symbols, gears };
  }

  async solveSilver(data: Awaited<ReturnType<Day3["prepareData"]>>) {
    const { numbers, symbols } = data;
    return numbers
      .filter((number) => {
        for (let x = number.x - 1; x <= number.x + number.width; x++) {
          for (let y = number.y - 1; y <= number.y + 1; y++) {
            if (symbols.has(coordKey(x, y))) return true;
          }
        }
        return false;
      })
      .reduce((a, b) => a + b.number, 0);
  }

  async solveGold(data: Awaited<ReturnType<Day3["prepareData"]>>) {
    const { numbers, gears } = data;

    const numberMap = new Map();

    numbers.forEach(({ x, y, width, number }) => {
      for (let i = 0; i < width; i++) {
        numberMap.set(coordKey(x + i, y), number);
      }
    });

    return gears
      .map((gear) => {
        const numbers = new Set<number>();
        for (let x = -1; x <= 1; x++) {
          for (let y = -1; y <= 1; y++) {
            const key = coordKey(gear.x + x, gear.y + y);
            if (numberMap.has(key)) numbers.add(numberMap.get(key));
          }
        }

        return numbers;
      })
      .filter((entry) => entry.size === 2)
      .map((entry) => [...entry.values()].reduce((a, b) => a * b, 1))
      .reduce(reduceSum);
  }
}
