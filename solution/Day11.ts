import Solution from "./Solution.js";
import { pairs } from "./utils/List.js";
import { manhattonDistance, Vector2 } from "./utils/Vector.js";

export default class Day11 extends Solution {
  async prepareData(input: string, isSilver: boolean) {
    const growRate = isSilver ? 1 : 999_999;

    const galaxies: Vector2[] = [];

    const lines = input.split("\n");
    const height = lines.length;
    const width = lines[0].length;
    lines.forEach((line, y) => {
      line.split("").forEach((value, x) => {
        if (value === "#") galaxies.push({ x, y });
      });
    });

    const rowsWithGalaxies = new Set();
    const colsWithGalaxies = new Set();

    // Find the rows/cols with galaxies
    for (const galaxy of galaxies) {
      rowsWithGalaxies.add(galaxy.y);
      colsWithGalaxies.add(galaxy.x);
    }

    // calculate modifiers
    const rowModifier = Array(height);
    let cRowModifier = 0;
    for (let y = 0; y < height; y++) {
      if (!rowsWithGalaxies.has(y)) cRowModifier += growRate;
      rowModifier[y] = cRowModifier;
    }

    const columnModifier = Array(width);
    let cColumnModifier = 0;
    for (let x = 0; x < width; x++) {
      if (!colsWithGalaxies.has(x)) cColumnModifier += growRate;
      columnModifier[x] = cColumnModifier;
    }

    // expand Galaxy
    for (const galaxy of galaxies) {
      galaxy.x += columnModifier[galaxy.x];
      galaxy.y += rowModifier[galaxy.y];
    }

    return galaxies;
  }

  async solveSilver(data: Awaited<ReturnType<Day11["prepareData"]>>) {
    let sum = 0;
    for (const [a, b] of pairs(data)) {
      sum += manhattonDistance(a, b);
    }
    return sum;
  }

  async solveGold(data: Awaited<ReturnType<Day11["prepareData"]>>) {
    let sum = 0;
    for (const [a, b] of pairs(data)) {
      sum += manhattonDistance(a, b);
    }
    return sum;
  }
}
