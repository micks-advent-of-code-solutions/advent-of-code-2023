import Solution from "./Solution.js";
import { reduceSum } from "./utils/Math.js";
import { intersection } from "./utils/Set.js";

export default class Day4 extends Solution {
  async prepareData(input: string) {
    return input.split("\n").map((line, i) => {
      const [winning, numbers] = line
        .split(": ")
        .pop()
        .split(" | ")
        .map(
          (numberLine) =>
            new Set(
              numberLine
                .trim()
                .split(/\W+/)
                .map((num) => parseInt(num)),
            ),
        );

      return { card: i + 1, winning: winning, numbers, amount: 1 };
    });
  }

  async solveSilver(data: Awaited<ReturnType<Day4["prepareData"]>>) {
    return data
      .map((card) => {
        const winningCount = intersection(card.winning, card.numbers).size;
        return winningCount ? 2 ** (winningCount - 1) : 0;
      })
      .reduce(reduceSum);
  }

  async solveGold(data: Awaited<ReturnType<Day4["prepareData"]>>) {
    for (const [i, card] of data.entries()) {
      const winningCount = intersection(card.winning, card.numbers).size;
      for (
        let j = i + 1;
        j < Math.min(data.length, i + winningCount + 1);
        j++
      ) {
        data[j].amount += card.amount;
      }
    }
    return data.reduce((a, b) => a + b.amount, 0);
  }
}
