export function GCD(x: number, y: number) {
  let a = x,
    b = y,
    bNew = 0;
  while (b !== 0) {
    bNew = a % b;
    a = b;
    b = bNew;
  }
  return a;
}

export function LCM(x: number, y: number) {
  return x * Math.abs(y / GCD(x, y));
}

export function reduceSum(a: number, b: number) {
  return a + b;
}

export function reduceProduct(a: number, b: number) {
  return a * b;
}

/**
 * https://en.wikipedia.org/wiki/Arithmetic_progression#Sum
 */
export function arithmeticSeries(from: number, to: number, step: number = 1) {
  return (((to - from) / step + 1) * (from + to)) / 2;
}
