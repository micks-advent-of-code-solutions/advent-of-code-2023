export function groupBy<K, V>(entries: V[], mapper: (V) => K = (i) => i) {
  const result = new Map<K, V[]>();
  for (const entry of entries) {
    const group = mapper(entry);
    if (!result.has(group)) {
      result.set(group, [entry]);
      continue;
    }
    result.get(group).push(entry);
  }
  return result;
}

export function* pairs<T>(entries: T[]) {
  const count = entries.length;
  for (let i = 0; i < count; i++) {
    for (let j = i + 1; j < count; j++) {
      yield [entries[i], entries[j]];
    }
  }
}
