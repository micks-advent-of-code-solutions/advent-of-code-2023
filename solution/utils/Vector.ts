export type Vector2 = { x: number; y: number };

export function manhattonDistance(a: Vector2, b: Vector2) {
  return Math.abs(a.x - b.x) + Math.abs(a.y - b.y);
}

export function toId(i: Vector2) {
  return i.x + "/" + i.y;
}

export function fromId(id: string) {
  const [x, y] = id.split("/").map((i) => parseFloat(i));
  return { x, y } as Vector2;
}
